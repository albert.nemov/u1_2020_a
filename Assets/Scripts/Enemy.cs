﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform player;
    public int hp;
    public int force;
    public float speed;
    public float distanceActivate;
    public float distanceAttack;
    public Animator animator;

    public Transform[] points;
    private int curIndToPoint = 0;
    private bool isIdle;

    private bool isFight;

    private void Start()
    {
        StartCoroutine(Fight());
    }
    public IEnumerator Fight()
    {
        while (true)
        {
            if (isFight)
            {
                player.GetComponent<Controller>().hp -= force;
            }
            yield return new WaitForSeconds(2);
        }
    }

    public IEnumerator PointControll()
    {
        yield return new WaitForSeconds(3);
        curIndToPoint++;
        if (curIndToPoint >= points.Length)
        {
            curIndToPoint = 0;
        }
        isIdle = false;
    }

    private void Update()
    {
        if (hp <= 0)
        {
            isFight = false;
            animator.SetBool("IsWalk", false);
            animator.SetBool("IsAttack", false);
            animator.SetBool("IsDead", true);
            Destroy(gameObject, 2);
        }
        else
        {

            float distanceFromPlayer =
                Vector3.Distance(player.position, transform.position);
            float distanceFromPoint =
                Vector3.Distance(points[curIndToPoint].position, transform.position);


            if (distanceFromPlayer <= distanceAttack)
            {
                isFight = true;
                animator.SetBool("IsAttack", true);
                animator.SetBool("IsWalk", false);
            }
            else
            {
                isFight = false;
                animator.SetBool("IsAttack", false);
                if (distanceFromPlayer <= distanceActivate && distanceFromPlayer > 2)
                {
                    Vector3 look = new Vector3(player.position.x, transform.position.y, player.position.z);
                    transform.LookAt(look);
                    transform.Translate(Vector3.forward * Time.deltaTime * speed);
                    animator.SetBool("IsWalk", true);
                }
                else
                {
                    if (isIdle)
                    {
                        animator.SetBool("IsWalk", false);
                    }
                    else
                    {
                        Vector3 look = new Vector3(points[curIndToPoint].position.x, transform.position.y, points[curIndToPoint].position.z);
                        transform.LookAt(look);
                        transform.Translate(Vector3.forward * Time.deltaTime * speed);
                        animator.SetBool("IsWalk", true);

                        if (distanceFromPoint < 2)
                        {
                            isIdle = true;
                            StartCoroutine(PointControll());
                        }
                    }
                }
            }
        }
    }
}
