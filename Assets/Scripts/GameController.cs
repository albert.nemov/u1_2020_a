﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Quaternion rot = transform.rotation;
        rot.z -= h * 0.01f;
        rot.x -= v * 0.01f;
        transform.rotation = rot;

    }
}
