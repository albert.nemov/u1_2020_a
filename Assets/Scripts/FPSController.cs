﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{
	public Transform head;
	private float rotationY;
	private Vector3 direction;
	private void Start()
    {
    }
    private void Update()
    {
		var h = Input.GetAxis("Horizontal");
		var v = Input.GetAxis("Vertical");

		// управление головой (камерой)
		float rotationX = head.localEulerAngles.y + Input.GetAxis("Mouse X") * 5;
		rotationY += Input.GetAxis("Mouse Y") * 5;
		rotationY = Mathf.Clamp(rotationY, -60, 60);
		head.localEulerAngles = new Vector3(-rotationY, rotationX, 0);

		// вектор направления движения
		direction = new Vector3(h, 0, v);
		direction = head.TransformDirection(direction);
		direction = new Vector3(direction.x, 0, direction.z);

		transform.Translate(direction * Time.deltaTime * 5);
	}
}
