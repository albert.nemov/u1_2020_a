﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllLevel : MonoBehaviour
{
    private int countKills;

    public static int killsForWin;

    private void Start()
    {
        countKills = 0;
    }

    public void AddKills()
    {
        countKills++;

        if (countKills >= killsForWin)
        {
            Debug.Log("Your Win");
        }
    }
}
