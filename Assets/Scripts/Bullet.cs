﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int timeDestroy;
    private void Start()
    {
        StartCoroutine(TimeDestroy(timeDestroy));
    }

    IEnumerator TimeDestroy(int time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Body")
        {
            Transform enemy = other.transform.parent.parent.parent.parent;
            enemy.GetComponent<Enemy>().hp -= 25;
            Destroy(gameObject);
        }

        if (other.tag == "Head")
        {
            Transform enemy = other.transform.parent.parent.parent.parent.parent.parent;
            enemy.GetComponent<Enemy>().hp -= 50;
            Destroy(gameObject);
        }
    }
}
