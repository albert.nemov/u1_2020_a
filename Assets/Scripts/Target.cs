﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public int level;
    public int speed;
    public int speedY;
    public int direction;
    private Animator animator;
    private TargetGenerator targetGenerator;

    private int minSpeed = 1;
    private int maxSpeed = 4;

    public void SettingTarget(TargetGenerator targetGenerator, 
        int minSpeed, int maxSpeed)
    {
        this.targetGenerator = targetGenerator;
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        level = 1;
        direction = 1;
        speed = 1;
        speedY = 1;
        StartCoroutine(ChangeData());
    }

    private IEnumerator ChangeData()
    {
        while (true)
        {
            float waitTime = Random.Range(1, 4);
            speed = Random.Range(minSpeed, maxSpeed);
            speedY = Random.Range(minSpeed, maxSpeed);
            direction *= -1;
            yield return new WaitForSeconds(waitTime);
        }
    }

    private void Update()
    {
        if (level == 2)
        {
            float deltaX = speed * direction * Time.deltaTime;
            float newX = transform.position.x + deltaX;
            if (newX > -10 && newX < 10)
            {
                transform.position += new Vector3(deltaX, 0, 0);
            }
            else
            {
                direction *= -1;
            }
        }
        else if (level == 3)
        {
            float deltaY = speedY * direction * Time.deltaTime;
            float newY = transform.position.y + deltaY;
            if (newY > 0 && newY < 15)
            {
                transform.position += new Vector3(0, deltaY, 0);
            }
            else
            {
                direction *= -1;
            }
        }
        else if (level == 4)
        {
            float deltaX = speed * direction * Time.deltaTime;
            float newX = transform.position.x + deltaX;
            float deltaY = speedY * direction * Time.deltaTime;
            float newY = transform.position.y + deltaY;
            if (newX > -10 && 
                newX < 10 && 
                newY > 0 && 
                newY < 15)
            {
                transform.position += new Vector3(deltaX, deltaY, 0);
            }
            else
            {
                direction *= -1;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            Destroy(other.gameObject);
            level++;

            if (level == 5)
            {
                animator.Play("TargetDeadAnimation");
                Destroy(gameObject, 1);
                targetGenerator.DeadTarget();
            }
            else
            {
                animator.Play("TargetAnimation", -1, 0);
            }
        }
    }
}
