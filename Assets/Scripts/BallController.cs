﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{
    public int countTeapots;
    public Text textScore;

    private void Start()
    {
        countTeapots = 0;
        textScore.text = "x0";
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Teapot")
        {
            countTeapots++;
            textScore.text = "x" + countTeapots;
            other.gameObject.SetActive(false);
        }
    }
}
