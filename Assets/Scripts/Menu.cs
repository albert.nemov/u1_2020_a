﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject panelMenu;
    public GameObject panelLevels;
    public GameObject panelAuthors;
    public void ButtonBackToMenu()
    {
        panelAuthors.SetActive(false);
        panelLevels.SetActive(false);
        panelMenu.SetActive(true);
    }
    public void ButtonOpenLevels()
    {
        panelAuthors.SetActive(false);
        panelLevels.SetActive(true);
        panelMenu.SetActive(false);
    }

    public void ButtonOpenAuthors()
    {
        panelAuthors.SetActive(true);
        panelLevels.SetActive(false);
        panelMenu.SetActive(false);
    }

    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void LoadLevel2()
    {
        SceneManager.LoadScene("Level2");
    }


    public void Exit()
    {
        Application.Quit();
    }
}
