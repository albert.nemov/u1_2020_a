﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingWeapon : MonoBehaviour
{
    public string nameWeapon;
    public Transform endWeapon;
    public int forceShot;
    public int ammo;
    public int maxSizeAmmo;
    public float timeReload;
}
