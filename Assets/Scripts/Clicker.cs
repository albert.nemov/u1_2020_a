﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clicker : MonoBehaviour
{
    private int countClicks;
    public Text textClicks;
    public Text textMoney;

    public int money;

    private void Start()
    {
        if (PlayerPrefs.HasKey("Clicks"))
        {
            countClicks = PlayerPrefs.GetInt("Clicks");
        }
        else
        {
            countClicks = 0;
        }

        if (PlayerPrefs.HasKey("Money"))
        {
            money = PlayerPrefs.GetInt("Money");
        }
        else
        {
            money = 0;
        }

        textMoney.text = money + "$";
        textClicks.text = countClicks.ToString();
    }

    public void Click()
    {
        countClicks++;
        money = countClicks / 10 * 5;
        textMoney.text = money + "$";
        textClicks.text = countClicks.ToString();
        PlayerPrefs.SetInt("Clicks", countClicks);
        PlayerPrefs.SetInt("Money", money);
    }
}
