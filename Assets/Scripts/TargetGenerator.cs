﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetGenerator : MonoBehaviour
{
    private int countLiveTargets;
    public int maxCountTargets;
    public GameObject prefabTarget;

    public float max;
    public float min;

    public static int minSpeed = 1;
    public static int maxSpeed = 3;

    private int countTargetKills;
    public Text textKills;

    public void DeadTarget()
    {
        countLiveTargets--;
        countTargetKills++;

        if (countLiveTargets < 1)
        {
            Spawn();
        }
    }

    private void Start()
    {
        Spawn();
    }

    private void Spawn()
    {
        for (int i = 0; i < maxCountTargets; i++)
        {
            float xRand = Random.Range(min, max);
            float yRand = Random.Range(min, max);
            float zRand = Random.Range(min, max);
            Vector3 newPos = new Vector3(xRand, yRand, zRand);
            newPos = newPos + transform.position;
            GameObject target = Instantiate(prefabTarget, newPos, Quaternion.identity);
            target.GetComponent<Target>().SettingTarget(this, minSpeed, maxSpeed);
        }
        countLiveTargets = maxCountTargets;
    }

    private void Update()
    {
        textKills.text = "Сбитых мешений: " + countTargetKills;
    }
}
